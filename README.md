# **Задание**:

**Решить задачу из двух частей и организовать структуру проекта на сервисах Gitlab.**

## **Задачи**:

1. *Для первой части организовать проект с системой контроля версий и систему сборки на основе CI/CD.* 
	Система CI/CD должна собирать приложение запускать его передавая аргумент, который может динамически указать пользователь. Результат работы приложения должен быть возвращен из CI/CD в виде артефакта.
2. *Для второй части организовать проект с системой контроля версий и систему сборки на основе CI/CD.* 
	Проект должен состоять из 2-х сервисов: сервиса «приложения» и сервиса с «данными». Сервис «приложения», должен делать запрос в сервис с «данными». Cистема CI/CD должна собирать приложение и подготовить все для запуска или получения результата его работы (Например, собрать и запустить сервисы в разных окружениях, где приложение делает запрос к данным и результат сохраняет в виде артефакта или подготовить файл для запуска сервисов «docker-compose.yml», которые сохраняют результат в файле).

## **Функциональные требования:**

1. Решить задачи на любом языке программирования.
2. Решения всех задач должны собираться, запускаться или разворачиваться системой CI/CD.
3. Для 1 части, требуется ввод данных, организовать данный ввод динамически средствами CI/CD (env переменные), а не «жестко закодировать» в приложении.
4. Для 2 части данные для задачи и приложение должны быть разделены на отдельные изолированные друг от друга сервисы. Например, сервис «БД» и сервис «приложения», которое делает запрос к «БД».
5. Данные для части 2 могут храниться в файле или БД.