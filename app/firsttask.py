import os

num = os.getenv("NUMBER", None)
exponent = os.getenv("EXPONENT", None)

num = int(num)
exponent = float(exponent)

if num is not None and exponent is not None:
    result = pow(num, exponent)
    print(f"{num} в степени {exponent} равно {result}")
else:
    print("Некорректные значения переменных среды.")
