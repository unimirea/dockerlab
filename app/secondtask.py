import os
import psycopg2


def conn(query: str):
    try:
        connection = psycopg2.connect(os.getenv("POS_URL"))
        cursor = connection.cursor()
        cursor.execute(query)
        worst_student = cursor.fetchone()
        cursor.close()
        connection.close()
        return worst_student
    except Exception as e:
        print(e)


if __name__ == '__main__':
    query = """
    SELECT s.name, s.surname, AVG(rc.mark) AS average_mark
    FROM students s
    LEFT JOIN report_card rc ON s.id = rc.id_student
    GROUP BY s.id, s.name, s.surname
    ORDER BY average_mark ASC NULLS LAST
    LIMIT 1;
    """

    worst_student = conn(query)

    if worst_student:
        print(
            f"Наихудшая средняя успеваемость: {worst_student[0]} {worst_student[1]} со среднем баллом {round(worst_student[2], 2)}"
        )
    else:
        print("Данные не найдены.")
